from flask import Flask
from flask import request
app = Flask(__name__)


@app.route('/login/')
def login():
    username = request.args.get('username', '')
    password = request.args.get('password', '')
    return 'Success' if username == 'user' and password == 'pass' else 'Wrong'


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')